import { useState, useEffect } from "react";
import axios from "axios";

// https://api.winihost.com

const useFetch = (endpoint) => {
  const [data, setData] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState(null);

  const options = {
    method: "POST",
    url: `https://api.winihost.com/${endpoint}`,
  };

  const fetchData = async () => {
    setIsLoading(true);

    try {
      const response = await axios.request(options);
      const responseData = response.data;
      const dataResult = responseData.result;

      setData(dataResult);
      setIsLoading(false);
    } catch (error) {
      setError(error);

      alert("Il y a une erreure!");
    } finally {
      setIsLoading(false);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return { data, isLoading, error };
};

export default useFetch;
