import {
  View,
  Text,
  TouchableOpacity,
  FlatList,
  ActivityIndicator,
  StyleSheet,
  Image,
} from "react-native";
import { useState } from "react";

import useFetch from "../../hook/useFetch";
import { useSelector } from "react-redux";

const ServiceDetails = () => {
  const { data, isLoading, error } = useFetch(
    "service/detail/24cb5f0f-0d89-49e2-89ad-4c224af8d4e1"
  );

  // -------------------------
  // to show each card
  // -------------------------

  const ServiceDetailsCard = ({ serviceCard }) => {
    return (
      <View style={styles.ccontainer}>
        <Image
          resizeMode="contain"
          style={styles.pictureContainer}
          source={serviceCard.photo}
        />
        <Text style={styles.cardText}>{serviceCard.name}</Text>
        {/* ---------details--------- */}
        <View style={styles.details}>
          <Text>Espace disque (Mo)</Text>
          <Text>{serviceCard.disk_quota}</Text>
        </View>
        <View style={styles.details}>
          <Text>Bande passante (Mo)</Text>
          <Text>{serviceCard.bandwidth}</Text>
        </View>
        <View style={styles.details}>
          <Text>Compte FTP</Text>
          <Text>{serviceCard.ftp_accounts}</Text>
        </View>
        <View style={styles.details}>
          <Text>Compte email</Text>
          <Text>{serviceCard.email_accounts}</Text>
        </View>
        <View style={styles.details}>
          <Text>Base de donnée (MySql)</Text>
          <Text>{serviceCard.databases}</Text>
        </View>
        <View style={styles.details}>
          <Text>Sous domaine</Text>
          <Text>{serviceCard.sub_domains}</Text>
        </View>
        <View style={styles.details}>
          <Text>Domaine principal</Text>
          <Text>{serviceCard.addons_domains}</Text>
        </View>
        <View style={styles.details}>
          <Text>Accès SSH</Text>
          <Text>{serviceCard.ssh_access}</Text>
        </View>
        {/* ----------------------------  */}

        <View>
          <Text style={styles.cardPrice}>
            {serviceCard.price} FCFA
            <Text style={styles.monthText}>/ Mois</Text>
          </Text>
        </View>
        <View style={styles.BtnArea}>
          <TouchableOpacity style={styles.Btn1}>
            <Text style={{ color: "black", fontWeight: "bold" }}>
              Ajouter au panier
            </Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.Btn2}>
            <Text style={{ color: "white", fontWeight: "bold" }}>Partager</Text>
          </TouchableOpacity>
        </View>
      </View>
      // <View>
      //   <Text>
      //     <Text>test</Text>
      //     <Text>{props.serviceCard.name}</Text>
      //   </Text>
      // </View>
    );
  };

  return (
    <View style={styles.container}>
      {isLoading ? (
        <ActivityIndicator size="small" color="blue" />
      ) : (
        <View>
          <FlatList
            data={data.packages}
            renderItem={({ item, key }) => (
              <ServiceDetailsCard key={key} serviceCard={item} />
            )}
            contentContainerStyle={{ columnGap: 15 }}
            horizontal
          />
        </View>
      )}
    </View>
  );
};

export default ServiceDetails;

const styles = StyleSheet.create({
  container: {
    marginTop: 0,
  },

  cardsContainer: {
    marginTop: 10,
  },
  ccontainer: {
    width: 315,
    padding: 20,
    backgroundColor: "white",
    borderRadius: 5,
    justifyContent: "space-between",
    borderWidth: 1,
    borderColor: "red",
  },

  pictureContainer: {
    display: "flex",
    width: "100%",
    height: 150,
    backgroundColor: "yellow",
    justifyContent: "center",
    alignItems: "center",
  },
  details: {
    borderWidth: 0.5,
    padding: 20,
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
  },
  cardText: {
    marginVertical: 15,
    textAlign: "center",
    fontWeight: "bold",
    color: "gray",
  },
  cardPrice: {
    marginVertical: 15,
    textAlign: "center",
    fontWeight: "bold",
    color: "black",
  },
  monthText: {
    fontWeight: 200,
  },
  BtnArea: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
  },

  Btn1: {
    padding: 10,
    backgroundColor: "orange",
  },
  Btn2: {
    paddingVertical: 10,
    paddingHorizontal: 20,
    backgroundColor: "gray",
    color: "white",
  },
});
