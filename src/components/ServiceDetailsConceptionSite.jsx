import {
  View,
  Text,
  TouchableOpacity,
  FlatList,
  ActivityIndicator,
  StyleSheet,
  Image,
} from "react-native";
import { useState } from "react";

import useFetch from "../../hook/useFetch";

const ServiceDetailsConceptionSite = (props) => {
  const slug = "website";
  props.onChildData(slug);

  const { data, isLoading, error } = useFetch(
    "service/detail/24cb5f0f-0d89-49e2-89ad-4c224af8d4e7"
  );

  // -------------------------
  // to show each card
  // -------------------------

  const ServiceDetailsCard = (props) => {
    return (
      <View style={styles.ccontainer}>
        <Image
          resizeMode="contain"
          style={styles.pictureContainer}
          source={props.serviceCard.photo}
        />
        <Text style={styles.cardText}>{props.serviceCard.name}</Text>
        {/* ---------details--------- */}
        <View style={styles.details}>
          <Text>Logo</Text>
          <Text>{props.serviceCard.logo}</Text>
        </View>
        <View style={styles.details}>
          <Text>Création pages sociales</Text>
          <Text>{props.serviceCard.social_page}</Text>
        </View>
        <View style={styles.details}>
          <Text>Nom de domaine</Text>
          <Text>{props.serviceCard.domain_name}</Text>
        </View>
        <View style={styles.details}>
          <Text>Espace d'administration</Text>
          <Text>{props.serviceCard.space_admin}</Text>
        </View>
        <View style={styles.details}>
          <Text>Espace disque (Go)</Text>
          <Text>{props.serviceCard.disk_space}</Text>
        </View>
        <View style={styles.details}>
          <Text>Carte de visite</Text>
          <Text>{props.serviceCard.business_card}</Text>
        </View>
        <View style={styles.details}>
          <Text>Emails pro</Text>
          <Text>{props.serviceCard.emails}</Text>
        </View>
        <View style={styles.details}>
          <Text>Responsive</Text>
          <Text>{props.serviceCard.responsive}</Text>
        </View>
        <View style={styles.details}>
          <Text>Référencement naturel (SEO)</Text>
          <Text>{props.serviceCard.seo}</Text>
        </View>
        <View style={styles.details}>
          <Text>Newsletter</Text>
          <Text>{props.serviceCard.newsletter}</Text>
        </View>
        <View style={styles.details}>
          <Text>Multi-langue</Text>
          <Text>{props.serviceCard.multilingue}</Text>
        </View>
        <View style={styles.details}>
          <Text>Compte utilisateurs</Text>
          <Text>{props.serviceCard.account_user}</Text>
        </View>
        <View style={styles.details}>
          <Text>site.label.search</Text>
          <Text>{props.serviceCard.search}</Text>
        </View>
        <View style={styles.details}>
          <Text>Zoom produit</Text>
          <Text>{props.serviceCard.zoom_product}</Text>
        </View>
        <View style={styles.details}>
          <Text>Avis utilisateurs</Text>
          <Text>{props.serviceCard.user_reviews}</Text>
        </View>
        <View style={styles.details}>
          <Text>Commande et/ou réservation</Text>
          <Text>{props.serviceCard.ordering}</Text>
        </View>
        <View style={styles.details}>
          <Text>Paiement en ligne</Text>
          <Text>{props.serviceCard.payonline}</Text>
        </View>
        <View style={styles.details}>
          <Text>Live chat</Text>
          <Text>{props.serviceCard.live_chat}</Text>
        </View>
        <View style={styles.details}>
          <Text>Question Fréquentes (FAQ)</Text>
          <Text>{props.serviceCard.faq}</Text>
        </View>
        {/* ----------------------------  */}

        <View>
          <Text style={styles.cardPrice}>
            {props.serviceCard.price} FCFA
            <Text style={styles.monthText}>/ Mois</Text>
          </Text>
        </View>
        <View style={styles.BtnArea}>
          <TouchableOpacity style={styles.Btn1}>
            <Text style={{ color: "black", fontWeight: "bold" }}>
              Ajouter au panier
            </Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.Btn2}>
            <Text style={{ color: "white", fontWeight: "bold" }}>Partager</Text>
          </TouchableOpacity>
        </View>
      </View>
      // <View>
      //   <Text>
      //     <Text>test</Text>
      //     <Text>{props.serviceCard.name}</Text>
      //   </Text>
      // </View>
    );
  };

  return (
    <View style={styles.container}>
      {isLoading ? (
        <ActivityIndicator size="small" color="blue" />
      ) : (
        <View>
          <FlatList
            data={data.packages}
            renderItem={({ item, key }) => (
              <ServiceDetailsCard key={key} serviceCard={item} />
            )}
            contentContainerStyle={{ columnGap: 15 }}
            horizontal
          />
        </View>
      )}
    </View>
  );
};

export default ServiceDetailsConceptionSite;

const styles = StyleSheet.create({
  container: {
    marginTop: 0,
  },

  cardsContainer: {
    marginTop: 10,
  },
  ccontainer: {
    width: 315,
    padding: 20,
    backgroundColor: "white",
    borderRadius: 5,
    justifyContent: "space-between",
    borderWidth: 1,
    borderColor: "red",
  },

  pictureContainer: {
    display: "flex",
    width: "100%",
    height: 150,
    backgroundColor: "yellow",
    justifyContent: "center",
    alignItems: "center",
  },
  details: {
    borderWidth: 0.5,
    padding: 20,
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
  },
  cardText: {
    marginVertical: 15,
    textAlign: "center",
    fontWeight: "bold",
    color: "gray",
  },
  cardPrice: {
    marginVertical: 15,
    textAlign: "center",
    fontWeight: "bold",
    color: "black",
  },
  monthText: {
    fontWeight: 200,
  },
  BtnArea: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
  },

  Btn1: {
    padding: 10,
    backgroundColor: "orange",
  },
  Btn2: {
    paddingVertical: 10,
    paddingHorizontal: 20,
    backgroundColor: "gray",
    color: "white",
  },
});
