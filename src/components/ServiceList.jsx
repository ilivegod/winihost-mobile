import { useState } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  FlatList,
  ActivityIndicator,
  StyleSheet,
} from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import ServiceDetails from "../components/ServiceDetails";
import ServiceDetailsStockageCloudPrivé from "../components/ServiceDetailsStockageCloudPrivé";
import ServiceDetailsServerVPS from "./ServiceDetailsServerVPS";
import ServiceDetailsInfographie from "./ServiceDetailsInfographie";

import useFetch from "../../hook/useFetch";
import ServiceDetailsConceptionSite from "./ServiceDetailsConceptionSite";
import ServiceDetailsBuilderAPI from "./ServiceDetailsBuilderAPI";
import HerbegementPersoScreen from "../screens/HerbegementPersoScreen";

const ServiceList = ({ navigation }) => {
  const { data, isLoading, error } = useFetch("service/list");
  const [selectedService, setSelectedService] = useState(null);

  const ServiceItem = (props) => {
    return (
      <View>
        <TouchableOpacity
          style={styles.Btn}
          onPress={() => () => navigation.navigate("DetailScreen")}
        >
          <Text>{props.service.name.fr}</Text>
        </TouchableOpacity>
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <View>
        {isLoading ? (
          <ActivityIndicator size="small" color="black" />
        ) : (
          <View>
            <FlatList
              data={data}
              renderItem={({ item, key }) => (
                <ServiceItem key={item.uuid} service={item} />
              )}
              contentContainerStyle={{ rowGap: 20 }}
              showsHorizontalScrollIndicator={false}
            />
          </View>
        )}
        {/*  */}
      </View>
    </View>
  );
};

export default ServiceList;

const styles = StyleSheet.create({
  containerList: {
    flex: 1,
    marginTop: 0,
    width: "100%",
  },

  Btn: {
    width: 350,
    borderWidth: 1,
    borderColor: "white",
    paddingHorizontal: 10,
    paddingVertical: 18,
    borderRadius: 8,
    backgroundColor: "white",
  },
});
