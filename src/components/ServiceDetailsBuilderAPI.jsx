import {
  View,
  Text,
  TouchableOpacity,
  FlatList,
  ActivityIndicator,
  StyleSheet,
  Image,
} from "react-native";
import { useState } from "react";

import useFetch from "../../hook/useFetch";

const ServiceDetailsBuilderAPI = (props) => {
  const slug = "builder";
  props.onChildData(slug);

  const { data, isLoading, error } = useFetch(
    "service/detail/24cb5f0f-0d89-49e2-89ad-4c224af8d410"
  );

  // -------------------------
  // to show each card
  // -------------------------

  const ServiceDetailsCard = (props) => {
    return (
      <View style={styles.ccontainer}>
        <Image
          resizeMode="contain"
          style={styles.pictureContainer}
          source={props.serviceCard.photo}
        />
        <Text style={styles.cardText}>{props.serviceCard.name}</Text>
        {/* ---------details--------- */}
        <View style={styles.details}>
          <Text>Modules</Text>
          <View>
            {props.serviceCard.modules.map((module) => {
              return (
                <View key={module} style={{ paddingVertical: 4 }}>
                  <Text>{module}</Text>
                </View>
              );
            })}
            {/* <FlatList
              data={props.serviceCard.module}
              renderItem={({ item }) => <Text>{item}</Text>}
            /> */}
          </View>
        </View>

        {/* ----------------------------  */}

        <View>
          <Text style={styles.cardPrice}>
            {props.serviceCard.price} FCFA
            <Text style={styles.monthText}>/ Mois</Text>
          </Text>
        </View>
        <View style={styles.BtnArea}>
          <TouchableOpacity style={styles.Btn1}>
            <Text style={{ color: "black", fontWeight: "bold" }}>
              Ajouter au panier
            </Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.Btn2}>
            <Text style={{ color: "white", fontWeight: "bold" }}>Partager</Text>
          </TouchableOpacity>
        </View>
      </View>
      // <View>
      //   <Text>
      //     <Text>test</Text>
      //     <Text>{props.serviceCard.name}</Text>
      //   </Text>
      // </View>
    );
  };

  return (
    <View style={styles.container}>
      {isLoading ? (
        <ActivityIndicator size="small" color="blue" />
      ) : (
        <View>
          <FlatList
            data={data.packages}
            renderItem={({ item, key }) => (
              <ServiceDetailsCard key={key} serviceCard={item} />
            )}
            contentContainerStyle={{ columnGap: 15 }}
            horizontal
          />
        </View>
      )}
    </View>
  );
};

export default ServiceDetailsBuilderAPI;

const styles = StyleSheet.create({
  container: {
    marginTop: 0,
  },

  cardsContainer: {
    marginTop: 10,
  },
  ccontainer: {
    width: 315,
    padding: 20,
    backgroundColor: "white",
    borderRadius: 5,
    justifyContent: "space-between",
    borderWidth: 1,
    borderColor: "red",
  },

  pictureContainer: {
    display: "flex",
    width: "100%",
    height: 150,
    backgroundColor: "yellow",
    justifyContent: "center",
    alignItems: "center",
  },
  details: {
    borderWidth: 0.5,
    padding: 20,
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
  },
  cardText: {
    marginVertical: 15,
    textAlign: "center",
    fontWeight: "bold",
    color: "gray",
  },
  cardPrice: {
    marginVertical: 15,
    textAlign: "center",
    fontWeight: "bold",
    color: "black",
  },
  monthText: {
    fontWeight: 200,
  },
  BtnArea: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
  },

  Btn1: {
    padding: 10,
    backgroundColor: "orange",
  },
  Btn2: {
    paddingVertical: 10,
    paddingHorizontal: 20,
    backgroundColor: "gray",
    color: "white",
  },
});
