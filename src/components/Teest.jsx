import { View, Text } from "react-native";
import React from "react";

const Teest = (props) => {
  const someData = "This is data from the child component!";
  props.onChildData(someData);
  return (
    <View>
      <Text>testCard</Text>
    </View>
  );
};

export default Teest;
