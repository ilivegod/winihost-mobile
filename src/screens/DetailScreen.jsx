import { useState } from "react";
import {
  View,
  ScrollView,
  Text,
  StyleSheet,
  Image,
  ActivityIndicator,
  FlatList,
  TouchableOpacity,
  SafeAreaView,
} from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { createStackNavigator } from "@react-navigation/stack";

import ServiceDetailsServerVPS from "../components/ServiceDetailsServerVPS";
import ServiceDetails from "../components/ServiceDetails";

import AsyncStorage from "@react-native-async-storage/async-storage";

import useFetch from "../../hook/useFetch";

import { addToCart, removeFromCart } from "../../redux/CartReducer";
import { useSelector, useDispatch } from "react-redux";
import { Ionicons } from "@expo/vector-icons";
import { MaterialCommunityIcons } from "@expo/vector-icons";

const DetailScreen = ({ navigation, route }) => {
  const dispatch = useDispatch();

  const totalQuantity = useSelector((state) => state.cart.totalQuantity);
  const totalItems = useSelector((state) => state.cart.totalItems);
  const id = route.params;
  const { data, isLoading } = useFetch(`service/detail/${id}`);

  // No need for separate item state as data.packages provides service details
  // const [item, setItem] = useState([]);

  const ServiceDetailsCard = ({ serviceCard }) => {
    const handleAddToCart = () => {
      const cartItem = {
        id: serviceCard.uuid,
        name: serviceCard.name,
        price: serviceCard.price,
        quantity: 1,
        item: 1,
      };
      dispatch(addToCart(cartItem)); // Dispatch addToCart action with cart item object
    };

    return (
      <View style={styles.ccontainer}>
        <View>
          {/* <Image
            resizeMode="contain"
            style={styles.pictureContainer}
            source={{ uri: serviceCard.photo }}
          /> */}
          <Text style={styles.cardText}>{serviceCard.name}</Text>

          <View>
            <Text style={styles.cardPrice}>
              {serviceCard.price} FCFA
              <Text style={styles.monthText}>/ Mois</Text>
            </Text>
          </View>
          <View style={styles.BtnArea}>
            <TouchableOpacity style={styles.Btn1} onPress={handleAddToCart}>
              <Text
                style={{ color: "black", fontWeight: "bold", fontSize: 10 }}
              >
                Ajouter au panier
              </Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.Btn2}>
              <Text
                style={{ color: "white", fontWeight: "bold", fontSize: 10 }}
              >
                Voir les Détails
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  };

  return (
    <SafeAreaView>
      <View style={styles.topBar}>
        <TouchableOpacity onPress={navigation.goBack}>
          <Ionicons name="arrow-back-outline" size={24} color="black" />
        </TouchableOpacity>
        <Text style={{ color: "#3C44FF", fontWeight: "600", fontSize: 15 }}>
          Liste des Packs
        </Text>
        <TouchableOpacity
          onPress={() => navigation.navigate("Cart")}
          style={{
            display: "flex",
            flexDirection: "row",
          }}
        >
          <MaterialCommunityIcons name="cart" size={24} color="black" />
          <Text style={{ fontWeight: "700" }}>{totalItems}</Text>
        </TouchableOpacity>
      </View>
      <View>
        <View
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "flex-end",
          }}
        ></View>
        <View style={styles.container}>
          {isLoading ? (
            <ActivityIndicator
              size="small"
              color="blue"
              style={{ position: "relative", bottom: "30%" }}
            />
          ) : (
            <View>
              <FlatList
                data={data.packages}
                renderItem={({ item, key }) => (
                  <ServiceDetailsCard key={key} serviceCard={item} />
                )}
                numColumns={2}
              />
            </View>
          )}
        </View>
      </View>
    </SafeAreaView>
  );
};

export default DetailScreen;

const styles = StyleSheet.create({
  container: {
    display: "flex",
    backgroundColor: "#F2F2F2",
    height: "100%",
    justifyContent: "center",
    alignItems: "center",
    paddingHorizontal: 7,
  },

  ccontainer: {
    width: "48%",
    maxHeight: 300,
    margin: 5,
    padding: 20,
    backgroundColor: "white",
    borderRadius: 3,
    justifyContent: "space-between",
  },

  pictureContainer: {
    display: "flex",
    width: "100%",
    height: 150,
    borderWidth: 1,
    borderColor: "gray",
    justifyContent: "center",
    alignItems: "center",
  },
  details: {
    borderWidth: 0.5,
    padding: 20,
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
  },
  cardText: {
    marginVertical: 15,
    textAlign: "center",
    fontWeight: "bold",
    color: "gray",
  },
  cardPrice: {
    marginVertical: 15,
    textAlign: "center",
    fontWeight: "bold",
    color: "black",
  },
  monthText: {
    fontWeight: 200,
  },
  BtnArea: {
    display: "flex",
    flexDirection: "col",
    justifyContent: "space-between",
  },

  Btn1: {
    padding: 10,
    marginVertical: 10,
    backgroundColor: "orange",
  },
  Btn2: {
    paddingVertical: 10,
    paddingHorizontal: 10,
    backgroundColor: "gray",
    color: "white",
  },
  topBar: {
    paddingHorizontal: 33,
    paddingVertical: 20,
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
  },
});
