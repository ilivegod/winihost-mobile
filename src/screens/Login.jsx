import {
  View,
  Text,
  TextInput,
  StyleSheet,
  TouchableOpacity,
  ActivityIndicator,
} from "react-native";
import { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { loginUser } from "../../redux/LoginSlice";
import * as Network from "expo-network";
import Constants from "expo-constants";
import { showMessage, hideMessage } from "react-native-flash-message";
import AsyncStorage from "@react-native-async-storage/async-storage";

import { Ionicons } from "@expo/vector-icons";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { SafeAreaView } from "react-native-safe-area-context";

const Login = ({ navigation }) => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [ip, setIp] = useState("");
  const [user_agent, setUserAgent] = useState("");
  const [isLoggedIn, setIsLoggedIn] = useState(false);

  const [isLoading, setIsLoading] = useState(false);

  const dispatch = useDispatch();

  const storeLoginStatus = async () => {
    try {
      await AsyncStorage.setItem("loginStatus", JSON.stringify(isLoggedIn));
    } catch (error) {
      console.error("Error saving login status to AsyncStorage:", error);
    }
  };

  useEffect(() => {
    const getIpAddress = async () => {
      const ipaddress = await Network.getIpAddressAsync();
      setIp(ipaddress);
    };
    const getUserAgent = async () => {
      Constants.getWebViewUserAgentAsync().then((r) => {
        setUserAgent(r);
      });
    };
    getIpAddress();
    getUserAgent();
  }, []);

  const handleLogin = () => {
    setIsLoading(true);
    let loginData = {
      email,
      password,
      ip,
      user_agent,
    };
    dispatch(loginUser(loginData)).then((result) => {
      if (result.payload && result.payload.success) {
        setEmail("");
        setPassword("");
        setIsLoggedIn(true);
        storeLoginStatus();
        navigation.navigate("Home");
        showMessage({
          message: "Success!",
          description: "Connexion réussi",
          type: "success",
        });
        setIsLoading(false);
      } else if (result.payload) {
        // Error case - show error message from API
        showMessage({
          message: "Error",
          description: result.payload.errors.msg,
          type: "danger",
        });
        setIsLoading(false);
      } else
        showMessage({
          message: "Error",
          description: "Une erreur inattendue est apparue. Veuillez réessayer.",
          type: "danger",
        });
      setIsLoading(false);
    });
  };
  return (
    <>
      <SafeAreaView>
        <View style={styles.topBar}>
          <TouchableOpacity onPress={navigation.goBack}>
            <Ionicons name="arrow-back-outline" size={24} color="black" />
          </TouchableOpacity>
          <Text style={{ color: "#3C44FF", fontWeight: "700", fontSize: 20 }}>
            Connexion
          </Text>
          <View></View>
        </View>
        <View style={{ marginTop: "30%", padding: 5 }}>
          <TextInput
            style={styles.input}
            value={email}
            placeholder="Addresse Email"
            onChangeText={(text) => setEmail(text)}
            autoCapitalize="none"
          />
          <TextInput
            style={styles.input}
            value={password}
            placeholder="Mot de Passe"
            onChangeText={(text) => setPassword(text)}
            secureTextEntry
            autoCapitalize="none"
          />
          <TouchableOpacity
            onPress={handleLogin}
            style={{
              marginTop: 10,

              borderRadius: 20,
              padding: 15,
              backgroundColor: "#3C44FF",
              width: "95%",
              alignSelf: "center",
            }}
          >
            {isLoading ? (
              <ActivityIndicator />
            ) : (
              <Text
                style={{
                  color: "white",
                  fontWeight: "600",
                  textAlign: "center",
                }}
              >
                Connexion
              </Text>
            )}
          </TouchableOpacity>

          <TouchableOpacity
            style={{
              marginTop: 20,
              alignSelf: "center",
              position: "relative",
            }}
          >
            <Text style={{ fontSize: 15, fontWeight: "500" }}>
              {" "}
              Mot de passe oublié ?
            </Text>
          </TouchableOpacity>
        </View>
      </SafeAreaView>
      <View
        style={{
          display: "flex",
          flexDirection: "row",
          marginTop: 20,
          gap: 10,
          alignSelf: "center",
          position: "absolute",
          left: "-1",
          right: "-1px",
          bottom: 30,
        }}
      >
        <Text style={{ fontSize: 15, fontWeight: "500" }}>
          Vous n'avez pas de compte ?
        </Text>
        <TouchableOpacity onPress={() => navigation.navigate("SignUp")}>
          <Text style={{ fontSize: 15, fontWeight: "700", color: "#3C44FF" }}>
            S'inscrire
          </Text>
        </TouchableOpacity>
      </View>
    </>
  );
};

export default Login;

const styles = StyleSheet.create({
  input: {
    padding: 20,
    borderWidth: 1,
    borderColor: "#E7E7E7",
    borderRadius: 10,
    width: "95%%",
    alignSelf: "center",
    marginBottom: 15,
    backgroundColor: "#fff",
  },
  topBar: {
    paddingHorizontal: 33,
    paddingVertical: 20,
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
  },
});
