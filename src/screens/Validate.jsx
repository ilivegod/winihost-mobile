import { useState } from "react";
import { View, Text, TextInput, TouchableOpacity } from "react-native";

import { clearCart } from "../../redux/CartReducer";
import { useSelector, useDispatch } from "react-redux";

const Validate = ({ navigation }) => {
  const dispatch = useDispatch();
  const [number, onChangeInput] = useState("");

  const [value, onChangeText] = useState("");

  const handleValidate = () => {
    dispatch(clearCart());
    alert("Votre commande a été validé");
    navigation.navigate("Home");
  };
  return (
    <View>
      <Text>Veuillez saisir le lieu de livraison</Text>
      <TextInput
        editable
        onChangeText={(text) => onChangeText(text)}
        value={value}
        style={{ padding: 10, borderWidth: 1, padding: 20, marginTop: 45 }}
      />
      <TouchableOpacity
        onPress={handleValidate}
        style={{
          padding: 15,
          marginTop: 60,
          backgroundColor: "pink",
        }}
      >
        <Text style={{ color: "white", fontWeight: "bold" }}>Valider</Text>
      </TouchableOpacity>
    </View>
  );
};

export default Validate;
