import { useState } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  FlatList,
  ActivityIndicator,
  StyleSheet,
  SafeAreaView,
} from "react-native";
import React from "react";
import { useSelector, useDispatch } from "react-redux";

import useFetch from "../../hook/useFetch";

import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";

import HerbegementPersoScreen from "../screens/HerbegementPersoScreen";
import { Ionicons } from "@expo/vector-icons";
import { MaterialCommunityIcons } from "@expo/vector-icons";

const Home = ({ navigation }) => {
  const { data, isLoading, error } = useFetch("service/list");
  const [selectedService, setSelectedService] = useState(null);

  const dispatch = useDispatch();
  const totalQuantity = useSelector((state) => state.cart.totalQuantity);
  const totalItems = useSelector((state) => state.cart.totalItems);

  const ServiceItem = ({ service }) => {
    let dataTopass = service.uuid;

    return (
      <View>
        <TouchableOpacity
          style={styles.Btn}
          onPress={() => navigation.navigate("DetailScreen", dataTopass)}
        >
          <Text>{service.name.fr}</Text>
        </TouchableOpacity>
      </View>
    );
  };

  return (
    <SafeAreaView>
      <View style={styles.topBar}>
        <View></View>
        <Text
          style={{
            color: "#3C44FF",
            fontWeight: "600",
            fontSize: 15,
            alignSelf: "center",
          }}
        >
          Liste des Packs
        </Text>
        <TouchableOpacity
          onPress={() => navigation.navigate("Cart")}
          style={{
            display: "flex",
            flexDirection: "row",
          }}
        >
          <MaterialCommunityIcons name="cart" size={24} color="black" />
          <Text style={{ fontWeight: "700" }}>{totalItems}</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.container}>
        <View style={styles.containerList}>
          <View>
            {isLoading ? (
              <ActivityIndicator size="small" color="black" />
            ) : (
              <View
                style={{
                  alignItems: "center",
                }}
              >
                <FlatList
                  data={data}
                  renderItem={({ item, key }) => (
                    <ServiceItem key={item.uuid} service={item} />
                  )}
                  contentContainerStyle={{ rowGap: 20 }}
                  showsHorizontalScrollIndicator={false}
                />
              </View>
            )}
            {/*  */}
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
};

export default Home;

const styles = StyleSheet.create({
  container: {
    height: "100%",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#F2F2F2",
  },
  headerTitle: {
    color: "#1D1D1D",
    fontWeight: "800",
    fontSize: 20,
    alignItems: "center",
    marginBottom: 7,
  },
  headerSubTitle: {
    color: "black",
    fontWeight: "500",
    fontSize: 20,
    alignItems: "center",
    top: 80,
    position: "absolute",
  },
  containerList: {
    flex: 1,
    marginTop: 0,
    width: "100%",
  },

  Btn: {
    alignItems: "center",
    justifyContent: "center",
    width: 350,
    borderWidth: 1,
    borderColor: "white",
    paddingHorizontal: 10,
    paddingVertical: 18,
    borderRadius: 8,
    backgroundColor: "white",
  },
  topBar: {
    paddingHorizontal: 33,
    paddingVertical: 20,
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
  },
});
