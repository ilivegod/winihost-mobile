import { useState } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  FlatList,
  ActivityIndicator,
  StyleSheet,
  SafeAreaView,
} from "react-native";
import { useSelector, useDispatch } from "react-redux";
import { navigation } from "../../navigation/rootNavigation";

const Profile = () => {
  return (
    <View>
      <Text style={{ marginHorizontal: 10 }}>Pas encore membre?</Text>
      <TouchableOpacity
        onPress={() => navigation.navigate("SignUp")}
        style={{
          width: 350,
          marginTop: 20,
          borderRadius: 8,
          paddingHorizontal: 10,
        }}
      >
        <Text>S'inscrire</Text>
      </TouchableOpacity>
    </View>
  );
};

export default Profile;
