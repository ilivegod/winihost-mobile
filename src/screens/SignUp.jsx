import {
  View,
  Text,
  TextInput,
  StyleSheet,
  TouchableOpacity,
  ActivityIndicator,
} from "react-native";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { showMessage, hideMessage } from "react-native-flash-message";
import { signUpUser } from "../../redux/SignUpSlice";
import { SafeAreaView } from "react-native-safe-area-context";

import { Ionicons } from "@expo/vector-icons";
import { MaterialCommunityIcons } from "@expo/vector-icons";

const SignUp = ({ navigation }) => {
  const [last_name, setLast_name] = useState("");
  const [first_name, setFirst_name] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [country, setCountry] = useState("");
  const [city, setCity] = useState("");
  const [phone, setPhone] = useState("");
  const [code_sponsor, setCode_sponsor] = useState("");

  const [isLoading, setIsLoading] = useState(false);

  const dispatch = useDispatch();

  const handleSignUp = () => {
    setIsLoading(true);
    let userData = {
      last_name,
      first_name,
      country,
      city,
      phone,
      email,
      password,
      code_sponsor,
    };
    dispatch(signUpUser(userData)).then((result) => {
      if (result.payload && result.payload.success) {
        setLast_name("");
        setFirst_name("");
        setEmail("");
        setPassword("");
        setCountry("");
        setCity("");
        setPhone("");
        setCode_sponsor("");
        navigation.navigate("Login");

        showMessage({
          message: "Success!",
          description: "Votre compte a été créé avec succès. Connectez vous",
          type: "success",
        });
        setIsLoading(false);
      } else if (result.payload) {
        // Error case - show error message from API
        showMessage({
          message: "Error",
          description: result.payload.errors.msg,
          type: "danger",
        });
        setIsLoading(false);
      } else
        showMessage({
          message: "Error",
          description: "Une erreur inattendue est apparue. Veuillez réessayer.",
          type: "danger",
        });
      setIsLoading(false);
    });
  };

  return (
    <>
      <SafeAreaView>
        <View style={styles.topBar}>
          <TouchableOpacity onPress={navigation.goBack}>
            <Ionicons name="arrow-back-outline" size={24} color="black" />
          </TouchableOpacity>
          <Text style={{ color: "#3C44FF", fontWeight: "700", fontSize: 20 }}>
            Création de compte
          </Text>
          <View></View>
        </View>
        <View style={{ marginTop: "5%", padding: 5 }}>
          <TextInput
            style={styles.input}
            value={last_name}
            placeholder="Nom"
            onChangeText={(text) => setLast_name(text)}
            autoCapitalize="none"
          />
          <TextInput
            style={styles.input}
            value={first_name}
            placeholder="Prénoms"
            onChangeText={(text) => setFirst_name(text)}
            autoCapitalize="none"
          />
          <TextInput
            style={styles.input}
            value={email}
            placeholder="Addresse Email"
            onChangeText={(text) => setEmail(text)}
            autoCapitalize="none"
          />
          <TextInput
            style={styles.input}
            value={password}
            placeholder="Mot de Passe"
            onChangeText={(text) => setPassword(text)}
            secureTextEntry
            autoCapitalize="none"
          />
          <TextInput
            style={styles.input}
            value={country}
            placeholder="Pays (code. Ex: CI)"
            onChangeText={(text) => setCountry(text)}
            autoCapitalize="none"
          />
          <TextInput
            style={styles.input}
            value={city}
            placeholder={"Ville (Ex: Abidjan)"}
            onChangeText={(text) => setCity(text)}
          />
          <TextInput
            style={styles.input}
            value={phone}
            keyboardType="numeric"
            placeholder={"Numéro de téléphone"}
            onChangeText={(text) => setPhone(text)}
          />
          <TextInput
            style={styles.input}
            value={code_sponsor}
            placeholder={"Entrez votre Code de parrainage(Facultatif)"}
            onChangeText={(text) => setCode_sponsor(text)}
          />
          <TouchableOpacity
            onPress={handleSignUp}
            style={{
              marginTop: 10,

              borderRadius: 20,
              padding: 15,
              backgroundColor: "#3C44FF",
              width: "95%",
              alignSelf: "center",
            }}
          >
            {isLoading ? (
              <ActivityIndicator />
            ) : (
              <Text
                style={{
                  color: "white",
                  fontWeight: "600",
                  textAlign: "center",
                }}
              >
                Créer mon compte
              </Text>
            )}
          </TouchableOpacity>
        </View>
      </SafeAreaView>
      <View
        style={{
          display: "flex",
          flexDirection: "row",
          marginTop: 20,
          gap: 10,
          alignSelf: "center",
          position: "absolute",
          left: "-1",
          right: "-1px",
          bottom: 30,
        }}
      >
        <Text style={{ fontSize: 15, fontWeight: "500" }}>
          Vous avez un compte ?
        </Text>
        <TouchableOpacity onPress={() => navigation.navigate("Login")}>
          <Text style={{ fontSize: 15, fontWeight: "700", color: "#3C44FF" }}>
            Se connecter
          </Text>
        </TouchableOpacity>
      </View>
    </>
  );
};

export default SignUp;

const styles = StyleSheet.create({
  input: {
    padding: 16,
    borderWidth: 1,
    borderColor: "#E7E7E7",
    borderRadius: 10,
    width: "95%%",
    alignSelf: "center",
    marginBottom: 15,
    backgroundColor: "#fff",
  },
  topBar: {
    paddingHorizontal: 33,
    paddingVertical: 20,
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
  },
});
