import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";

import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  StyleSheet,
} from "react-native";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { showMessage, hideMessage } from "react-native-flash-message";
import { clearCart, removeFromCart } from "../../redux/CartReducer";

import { Ionicons } from "@expo/vector-icons";
import { MaterialCommunityIcons } from "@expo/vector-icons";

import { useSelector, useDispatch } from "react-redux";
import { SafeAreaView } from "react-native-safe-area-context";

const Cart = ({ navigation }) => {
  const dispatch = useDispatch();
  const [loggedInStatus, setloggedInStatus] = useState(false);

  const totalAmount = useSelector((state) => state.cart.totalAmount);
  const totalQuantity = useSelector((state) => state.cart.totalQuantity);

  const cartItems = useSelector((state) => state.cart.cartItems);

  useEffect(() => {
    const fetchLoginStatus = async () => {
      try {
        const value = await AsyncStorage.getItem("loginStatus");
        if (value !== null) {
          const isLoggedIn = JSON.parse(value);
          setloggedInStatus(isLoggedIn);
          return isLoggedIn;
        } else {
          return false;
        }
      } catch (error) {
        console.error(
          "Error retrieving login status from AsyncStorage:",
          error
        );
        return false;
      }
    };

    fetchLoginStatus();
  }, []);

  const ValidateCart = () => {
    if (loggedInStatus) {
      navigation.navigate("Validate");
    } else {
      showMessage({
        message: "Error!",
        description: "Connectez Vous!",
        type: "danger",
      });
      navigation.navigate("Login");
    }
  };

  const handleClearCart = () => {
    dispatch(clearCart());
  };

  const handleRemoveFromCart = (itemId) => {
    console.log(itemId);
    dispatch(removeFromCart(itemId));
  };

  return (
    <>
      <SafeAreaView>
        <View style={styles.topBar}>
          <TouchableOpacity onPress={navigation.goBack}>
            <Ionicons name="arrow-back-outline" size={24} color="black" />
          </TouchableOpacity>
          <Text style={{ color: "#3C44FF", fontWeight: "700", fontSize: 17 }}>
            Panier({totalQuantity})
          </Text>
          <View>
            <Text style={{ fontWeight: "600" }}>
              cfa <Text style={{ fontWeight: "800" }}>{totalAmount}</Text>
            </Text>
          </View>
        </View>
        <View>
          <View>
            {cartItems.map((item) => (
              <View
                key={item.id}
                style={{
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "center",
                  justifyContent: "space-between",
                  padding: 10,
                  marginVertical: 5,
                }}
              >
                <View>
                  <View>
                    <Text style={{ fontWeight: "700", fontSize: 16 }}>
                      {item.name}
                    </Text>
                  </View>
                  <TouchableOpacity
                    onPress={() => handleRemoveFromCart(item.id)}
                    style={{
                      display: "flex",
                      flexDirection: "row",
                      marginTop: 10,
                    }}
                  >
                    <Ionicons name="trash-sharp" size={12} color="gray" />
                    <Text style={{ color: "gray", fontSize: 12 }}>
                      Supprimer
                    </Text>
                  </TouchableOpacity>
                </View>
                <View
                  style={{
                    borderWidth: 1,
                    borderColor: "gray",
                    padding: 5,
                    borderRadius: 3,
                  }}
                >
                  <Text style={{ color: "gray" }}>{item.quantity}</Text>
                </View>
                <View
                  style={{
                    display: "flex",
                    flexDirection: "row",
                  }}
                >
                  <View>
                    <Text>cfa {item.price} </Text>
                  </View>
                </View>
              </View>
            ))}
          </View>
        </View>
      </SafeAreaView>
      <View style={{ position: "absolute", bottom: 20, width: "100%" }}>
        <TouchableOpacity
          onPress={handleClearCart}
          style={{
            position: "relative",
            margin: 20,
          }}
        >
          <Text style={{ textAlign: "center", color: "#3C44FF" }}>
            Vider mon panier
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={ValidateCart}
          style={{
            marginTop: 10,

            borderRadius: 20,
            padding: 15,
            backgroundColor: "#3C44FF",
            width: "95%",
            alignSelf: "center",
          }}
        >
          <Text
            style={{ color: "white", fontWeight: "600", textAlign: "center" }}
          >
            Valider mon panier
          </Text>
        </TouchableOpacity>
      </View>
    </>
  );
};

export default Cart;

const styles = StyleSheet.create({
  topBar: {
    paddingHorizontal: 33,
    paddingVertical: 20,
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
  },
});
