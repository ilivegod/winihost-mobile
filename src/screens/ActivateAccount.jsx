import {
  View,
  Text,
  TextInput,
  StyleSheet,
  TouchableOpacity,
} from "react-native";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { activateAccount } from "../../redux/AccountActivateSlice";

const ActivateAccount = ({ navigation }) => {
  const [token_activation, setToken_activation] = useState("");
  const [sendingCode, setSendingCode] = useState(false);

  const { loading, error, user, errorMessage } = useSelector(
    (state) => state.activateAccount
  );

  const dispatch = useDispatch();

  const handleSendCode = () => {
    let userCode = token_activation;
    dispatch(activateAccount(userCode))
      .then((result) => {
        if (result.payload && result.payload.success) {
          setToken_activation("");
          navigation.navigate("Login");
        } else {
          alert(" Code d'activation invalide ou manquant");
        }
      })
      .catch((error) => {
        console.error("Activation error:", error);
      });
  };

  return (
    <View>
      <TextInput
        style={styles.input}
        value={token_activation}
        placeholder="Code d'Activation"
        onChangeText={(text) => setToken_activation(text)}
        autoCapitalize="none"
      />
      <TouchableOpacity onPress={handleSendCode}>
        <Text>Activer mon compte</Text>
      </TouchableOpacity>
    </View>
  );
};

export default ActivateAccount;

const styles = StyleSheet.create({
  input: {
    height: 40,
    marginBottom: 10,
    backgroundColor: "#fff",
  },

  errorText: {
    color: "red",
    marginBottom: 10,
  },
});
