import * as React from "react";
import { StackActions } from "@react-navigation/native";

export const rootNavigationRef = React.createRef();

/**
 * @description Go to another screen, figures out the action it needs to take to do it
 * @param name
 * @param params
 */
export function navigate(name, params) {
  rootNavigationRef.current?.navigate(name, params);
}

/**
 * @description Send an action object to update the navigation state
 * @param action
 */
export function dispatch(action) {
  rootNavigationRef.current?.dispatch(action);
}

/**
 * @description Go to a specific screen in the tab navigator
 * @param name
 * @param params
 */
export function jumpTo(name, params) {
  rootNavigationRef.current?.jumpTo(name, params);
}

/**
 * @description Replace the current route with a new one
 * @param name
 * @param params
 */
export function replace(name, params) {
  rootNavigationRef.current?.dispatch(StackActions.replace(name, params));
}

/**
 * @description Push a new route onto the stack
 * @param name
 * @param params
 */
export function push(name, params) {
  rootNavigationRef.current?.dispatch(StackActions.push(name, params));
}

/**
 * @description Close active screen and move back in the stack
 * @description
 */
export function goBack() {
  rootNavigationRef.current?.goBack();
}

/**
 * @description Wipe the navigator state and replace it with a new route
 * @description
 */
export function reset() {
  rootNavigationRef.current?.reset();
}

/**
 * @description Make changes to route's params
 * @description
 */
export function setParams(params) {
  rootNavigationRef.current?.setParams(params);
}

/**
 * @description Update the screen's options
 * @description
 */
export function setOptions(options) {
  rootNavigationRef.current?.setOptions(options);
}

/**
 * @description subscribe to updates to events from the navigators
 * @description
 */
export function addListener() {
  rootNavigationRef.current?.addListener();
}
/**
 * @description Check whether the screen is focused
 * @description
 */
export function isFocused() {
  rootNavigationRef.current?.isFocused();
}

export const navigation = {
  navigate,
  dispatch,
  jumpTo,
  replace,
  push,
  setParams,
  isFocused,
  setOptions,
  goBack,
};
