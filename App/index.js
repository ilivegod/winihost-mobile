import * as React from "react";
import { View } from "react-native";
import { TouchableOpacity, Text } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { useNavigation } from "@react-navigation/native";
import { Provider } from "react-redux";
import FlashMessage from "react-native-flash-message";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import {
  rootNavigationRef,
  navigation as navigationRefRoot,
} from "../navigation/rootNavigation";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { Ionicons } from "@expo/vector-icons";

import Home from "../src/screens/Home";
import DetailScreen from "../src/screens/DetailScreen";
import store from "../redux/store";
import Cart from "../src/screens/Cart";
import Validate from "../src/screens/Validate";
import SignUp from "../src/screens/SignUp";
import ActivateAccount from "../src/screens/ActivateAccount";
import Login from "../src/screens/Login";
import Profile from "../src/screens/Profile";

import { useSelector } from "react-redux";

const Tab = createBottomTabNavigator();
const HomeStack = createStackNavigator();
const DefaultStack = createNativeStackNavigator();

function HomeStackScreen() {
  return (
    <HomeStack.Navigator>
      <HomeStack.Screen
        name="HomeStack"
        component={Home}
        options={({ navigation }) => ({
          title: "Home",
          headerStyle: { backgroundColor: "#F2F2F2" },
          headerShadowVisible: false,
          headerShown: false,
          headerTintColor: "blue",
        })}
      />
    </HomeStack.Navigator>
  );
}

function CartStackScreen() {
  return (
    <HomeStack.Navigator>
      <HomeStack.Screen
        name="CartStack"
        component={Cart}
        options={({ navigation }) => ({
          headerStyle: { backgroundColor: "#F2F2F2" },
          headerShadowVisible: false,
          headerShown: false,
          headerTintColor: "blue",
        })}
      />
    </HomeStack.Navigator>
  );
}

function ProfileStackScreen() {
  return (
    <HomeStack.Navigator>
      <HomeStack.Screen
        name="ProfileStack"
        component={Profile}
        options={({ navigation }) => ({
          headerStyle: { backgroundColor: "#F2F2F2" },

          headerShadowVisible: false,
          headerTintColor: "blue",
        })}
      />
    </HomeStack.Navigator>
  );
}

const DefaultTabNavigation = () => {
  return (
    <Tab.Navigator
      screenOptions={{
        headerShown: false,
        tabBarIcon: null,
      }}
    >
      <Tab.Screen
        name="Home"
        component={HomeStackScreen}
        options={{
          title: "Accueil",
          tabBarIcon: ({ focused }) => {
            return (
              <MaterialCommunityIcons
                name="home"
                size={24}
                color={focused ? "#3C44FF" : "gray"}
              />
            );
          },
        }}
      />

      <Tab.Screen
        name="Cart"
        component={CartStackScreen}
        options={{
          title: "Panier",
          tabBarIcon: ({ focused }) => {
            return (
              <MaterialCommunityIcons
                name="cart"
                size={24}
                color={focused ? "#3C44FF" : "gray"}
              />
            );
          },
        }}
      />
      <Tab.Screen
        name="Profile"
        component={ProfileStackScreen}
        options={{
          title: "Profile",
          tabBarIcon: ({ focused }) => {
            return (
              <Ionicons
                name="person-circle-outline"
                size={24}
                color={focused ? "#3C44FF" : "gray"}
              />
            );
          },
        }}
      />
    </Tab.Navigator>
  );
};

export default function App() {
  return (
    <Provider store={store}>
      <NavigationContainer ref={rootNavigationRef}>
        <DefaultStackNavigation />

        <FlashMessage position="top" />
      </NavigationContainer>
    </Provider>
  );
}

const DefaultStackNavigation = ({ navigation }) => (
  <DefaultStack.Navigator
    screenOptions={{
      headerShown: false,
    }}
  >
    <DefaultStack.Screen
      navigation={navigation}
      name="DefaultHome"
      component={DefaultTabNavigation}
      options={{
        headerStyle: { backgroundColor: "#F2F2F2" },
        headerShadowVisible: false,
      }}
    />
    <DefaultStack.Screen
      navigation={navigation}
      name="DetailScreen"
      component={DetailScreen}
      options={{
        headerStyle: { backgroundColor: "#F2F2F2" },
        headerShadowVisible: false,
        headerTintColor: "blue",
      }}
    />
    <DefaultStack.Screen
      name="Login"
      component={Login}
      navigation={navigation}
    />
    <DefaultStack.Screen name="SignUp" component={SignUp} />
    <DefaultStack.Screen name="ActivateAccount" component={ActivateAccount} />
    <DefaultStack.Screen name="Validate" component={Validate} />
  </DefaultStack.Navigator>
);
