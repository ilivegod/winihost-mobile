import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from "axios";

export const activateAccount = createAsyncThunk(
  "activateAccount/AccountActivation",
  async (userCode) => {
    console.log(userCode);
    try {
      const response = await axios.post(
        "https://api.winihost.com/auth/register/activate",
        { token_activation: userCode }
      );
      let data = response.data;
      let errorMessage = data.errors.msg;
      console.log(data);
      console.log(errorMessage);
    } catch (exception) {
      console.log(exception);
    }
  }
);

const activateAccountSlice = createSlice({
  name: "ActivateAccount",
  initialState: {
    loading: false,
    user: null,
    error: null,
  },
  extraReducers: (builder) => {
    builder
      .addCase(activateAccount.pending, (state) => {
        state.loading = true;
        state.user = null;
        state.error = null;
      })
      .addCase(activateAccount.fulfilled, (state, action) => {
        state.loading = false;
        state.user = action.payload;
        state.error = null;
      })
      .addCase(activateAccount.rejected, (state, action) => {
        state.loading = false;
        state.user = null;
        state.error = errorMessage;
      });
  },
});

export default activateAccountSlice.reducer;
