import { configureStore } from "@reduxjs/toolkit";
import cartReducer from "./CartReducer";
import signUpReducer from "./SignUpSlice";
import activateAccountReducer from "./AccountActivateSlice";
import loginReducer from "./LoginSlice";

const store = configureStore({
  reducer: {
    cart: cartReducer,
    signup: signUpReducer,
    activateAccount: activateAccountReducer,
    login: loginReducer,
  },
});

export default store;
