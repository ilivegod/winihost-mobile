import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  cartItems: [],
  totalQuantity: 0,
  totalAmount: 0,
  totalItems: 0,
};

export const cartSlice = createSlice({
  name: "cart",
  initialState,
  reducers: {
    addToCart(state, action) {
      const newItem = action.payload;
      const existingItemIndex = state.cartItems.findIndex(
        (item) => item.id === newItem.id
      );

      const updatedState = {
        ...state,
        totalQuantity: state.totalQuantity + 1,
        totalAmount: state.totalAmount + newItem.price,
        totalItems:
          existingItemIndex === -1 ? state.totalItems + 1 : state.totalItems,
      };

      const newState =
        existingItemIndex !== -1
          ? {
              ...updatedState,
              cartItems: state.cartItems.map((item, index) =>
                index === existingItemIndex
                  ? { ...item, quantity: item.quantity + 1 }
                  : item
              ),
            }
          : {
              ...updatedState,
              cartItems: [...state.cartItems, { ...newItem, quantity: 1 }],
            };

      return newState;
    },

    removeFromCart(state, action) {
      const itemId = action.payload;

      const existingItemIndex = state.cartItems.findIndex(
        (item) => item.id === itemId
      );
      if (existingItemIndex === -1) {
        return state;
      }

      const updatedCartItems = [...state.cartItems];

      updatedCartItems.splice(existingItemIndex, 1);

      const newTotalQuantity =
        state.totalQuantity -
        (state.cartItems[existingItemIndex] || {}).quantity;
      const newTotalAmount =
        state.totalAmount - (state.cartItems[existingItemIndex] || {}).price;
      const newTotalItems =
        state.totalItems - (state.totalItems[existingItemIndex] || {}).item;

      return {
        ...state,
        cartItems: updatedCartItems,
        totalAmount: newTotalAmount,
        totalQuantity: newTotalQuantity,
        totalItems: newTotalItems,
      };
    },

    clearCart() {
      return initialState; // Reset to initial state
    },
  },
});
export const { addToCart, removeFromCart, clearCart } = cartSlice.actions;
export default cartSlice.reducer;
