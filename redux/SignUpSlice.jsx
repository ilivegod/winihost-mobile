import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from "axios";

export const signUpUser = createAsyncThunk(
  "signup/signUpUser",
  async (userData) => {
    try {
      const response = await axios.post(
        "https://api.winihost.com/auth/register",
        userData,
        {
          headers: { "Content-Type": "application/json" },
        }
      );
      let dataResult = await response.data;
      let dataResultError = dataResult.errors.msg;
      console.log(dataResultError);
      return dataResult;
    } catch (exception) {
      console.log(exception);
    }
  }
);

const signUpSlice = createSlice({
  name: "SignUp",
  initialState: {
    loading: false,
    user: null,
    error: null,
  },
  extraReducers: (builder) => {
    builder
      .addCase(signUpUser.pending, (state) => {
        state.loading = true;
        state.user = null;
        state.error = null;
      })
      .addCase(signUpUser.fulfilled, (state, action) => {
        state.loading = false;
        state.user = action.payload;
        state.error = null;
      })
      .addCase(signUpUser.rejected, (state, action) => {
        state.loading = false;

        state.user = null;
        state.error = null;
      });
  },
});

export default signUpSlice.reducer;
