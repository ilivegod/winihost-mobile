import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from "axios";

export const loginUser = createAsyncThunk(
  "login/loginUser",
  async (loginData) => {
    console.log(loginData);
    try {
      const response = await axios.post(
        "https://api.winihost.com/auth/login",
        loginData,
        {
          headers: { "Content-Type": "application/json" },
        }
      );
      let data = await response.data;
      return data;
    } catch (exception) {
      console.log(exception);
    }
  }
);

const loginSlice = createSlice({
  name: "Login",
  initialState: {
    loading: false,
    user: null,
    error: null,
  },
});

export default loginSlice.reducer;
